# CTF SSH Cred check

Checks to see if all hosts in a csv file have their default SSH port open, and also if you can log in with provided credentials

# Usage

You'll need to create your own .csv file.  You can do this easily with excel or google sheets.  The first column is your team name, your second column is your hostname or ip.  The csv file must be named 'teams.csv'.