import socket, csv, paramiko


#checks to see if the port is open.
def check_ssh(server_ip, port=22):
    try:
        test_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        test_socket.connect((server_ip, port))
    except:
        # not up, log reason from ex if wanted
        return False
    else:
        test_socket.close()
    return True

#This creates the dictionary from the csv file.
def dict_from_csv():
    reader = csv.reader(open('teams.csv', 'r'))
    d = dict(reader)
    return d

def login_ssh(hostname):
    host=hostname
    port=22
    user="root"
    password="password"
    command = "whoami"
    try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, port, user, password)
    except paramiko.AuthenticationException:
        return False

    
    stdin, stdout, stderr = client.exec_command(command)
    lines = stdout.readlines()
    
    #checks to see if the output of 'whoami' is the same as the user used to login, accounts for newline character in stdout        
    if (lines[0] == user+'\n'):
        return True
    else:
        return False
    client.close()


def main():
    #calls a function to turn 'teams.csv' into a dictionary.
    teams_dict = dict_from_csv()
    up_dict = {}
    down_dict = {}
    vuln_dict = {}
    sec_dict = {}

    #calls a function to check to see if each team is up before attempting to log in
    print("Checking up status of teams...")
    for key in teams_dict:
        if check_ssh(teams_dict[key]) == True:
            print(key+ " is Up.")
            up_dict[key] = teams_dict[key]
        else:
            print(key+ " is Down.")
            down_dict[key] = teams_dict[key]
        
    print("Attempting login for hosts that are up...")
    #calls a function to try to log in for all up hosts
    for key in up_dict:

        if login_ssh(up_dict[key]) == True:
            print(key+" is vulnerable.")
            vuln_dict[key] = up_dict[key] 
        else:
            print(key+" is secure.")
            sec_dict[key] = up_dict[key]

    print("The following teams are down:\n")
    print(down_dict)
    print("The following teams are up:\n")
    print(up_dict)
    print("The following teams are vulnerable:\n")
    print(vuln_dict)
    print("The following hosts are secure:\n")
    print(sec_dict)



if __name__ == "__main__":
    main()
